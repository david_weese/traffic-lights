// File:   plant_waterer.cpp
// Author: David Weese <dave.weese@gmail.com>

// Low power code from https://github.com/rocketscream/Low-Power

#include <stdint.h>
#include <stdbool.h>

#include <util/delay.h>
#include <avr/io.h>

#include <avr/sleep.h>
#include <avr/wdt.h>
#include <avr/power.h>
#include <avr/interrupt.h>

#include <pwm.h>
#include <timer.h>
#include <static_scripts.h>
#include "predefined_scripts.h"

////////////////////////////////////////////////////////////////////////////////////////////
// CONFIG

#define LED_PIN DDB0
#define PUMP_DURATION_MS 4000
#define WTD_PERIOD WDTO_1S
#define WTD_PERIOD_S 1
#define REPEAT_INTERVAL_S 20 //24 * 60 * 60

////////////////////////////////////////////////////////////////////////////////////////////
// MACROS

#define adc_disable() (ADCSRA &= ~(1 << ADEN)) // disable ADC (before power-off)
#define turnOn(pin, duration) \
    {                         \
        PORTB |= _BV(pin);    \
        _delay_ms(duration);  \
        PORTB &= ~_BV(pin);   \
    }

////////////////////////////////////////////////////////////////////////////////////////////
// VARIABLES

volatile bool timeToPump = false;                // start with false as protection against reboot loops
unsigned long uptimeSeconds = REPEAT_INTERVAL_S; // we will pump at the first wakeup call
struct thread_t script_thread;

ISR(WDT_vect)
{
    // WDIE & WDIF is cleared in hardware upon entering this ISR
    // wdt_disable();
    WDTCR |= (1 << WDIE);
    turnOn(LED_PIN, 1);

    uptimeSeconds += WTD_PERIOD_S;
    while (uptimeSeconds >= REPEAT_INTERVAL_S)
    {
        uptimeSeconds -= REPEAT_INTERVAL_S;
        timeToPump = true;
    }
}

void enableWdt(unsigned char period)
{
    wdt_enable(period);
    WDTCR |= (1 << WDIE);
}

void enterSleep()
{
    cli();
    sleep_enable();
    sei();
    sleep_cpu();
    sleep_disable();
    sei();
}

void pumpAll(void)
{
    init_pwm();
    init_fader();

    init_script_thread(&script_thread);
    script_thread.handler.execute = &memory_handler_flash;
    script_thread.handler.position = (uint16_t)&pump_program;

    do
    {
        // fade and update pwm channels
        uint8_t i;
        channel_t *ch = channels;
        for (i = 0; i < CHANNELS; ++i, ++ch)
            if (timer_expired(&ch->fade_timer))
            {
                timer_set(&ch->fade_timer, ch->fade_delay);
                if (!update_lum(ch))
                    fading_done |= 1 << i;
            }
    } while (execute_script_thread(&script_thread) != OP_RETURN_STOP);
}

void setup()
{
    adc_disable(); // ADC uses ~320uA

    // set pins to output
    DDRB |= _BV(LED_PIN);

    set_sleep_mode(SLEEP_MODE_PWR_DOWN);
    enableWdt(WTD_PERIOD);
}

int main(void)
{
    ////////////////////////////////////////////////////////////////////////////////////////////
    // INITIALIZATION
    setup();

    ////////////////////////////////////////////////////////////////////////////////////////////
    // MAIN LOOP
    while (1)
    {
        if (timeToPump)
        {
            pumpAll();
            timeToPump = false;
        }
        else
        {
            // flash LED
            // turnOn(LED_PIN, 1);
        }
        enterSleep();
    }
}
