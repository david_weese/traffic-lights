#if STATIC_SCRIPTS

#include <static_scripts.h>

////////////////////////////////////////////////////////////////////////////////////////////
// PUMP SCRIPT
//
// Cycle through all pumps iteratively. The pumps are gradually turned on/off to
// limit reverse currents.

#define CHANNEL0        1
#define CHANNEL1        2
#define CHANNEL2        4

static const uint8_t pump_program[] PROGMEM = {

    SET_CHANNEL(CHANNEL0 | CHANNEL1 | CHANNEL2, 0),
    FADE_STEP(CHANNEL0, 2),
    FADE_CHANNEL(CHANNEL0, 255),
    SLEEP(100),
    FADE_CHANNEL(CHANNEL0, 0),
    SLEEP(5),

    FADE_STEP(CHANNEL1, 2),
    FADE_CHANNEL(CHANNEL1, 255),
    SLEEP(100),
    FADE_CHANNEL(CHANNEL1, 0),
    SLEEP(5),

    FADE_STEP(CHANNEL2, 2),
    FADE_CHANNEL(CHANNEL2, 255),
    SLEEP(100),
    FADE_CHANNEL(CHANNEL2, 0),
    SLEEP(5),
    
    STOP()
};

#endif
