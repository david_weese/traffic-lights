// File:   traffic_lights.cpp
// Author: David Weese <dave.weese@gmail.com>

#include <stdbool.h>
#include <avr/io.h>
#include <avr/interrupt.h>
#include <util/delay.h>

#include <timer.h>
#include <pwm.h>
#include <key.h>
#include <static_scripts.h>
#include "predefined_scripts.h"

int main(void)
{
    ////////////////////////////////////////////////////////////////////////////////////////////
    // INITIALIZATION

    init_pwm();
    init_fader();
    init_timer();
    init_keys();
    sei();

    struct thread_t script_thread;
    init_script_thread(&script_thread);
    script_thread.handler.execute = &memory_handler_flash;
    script_thread.handler.position = (uint16_t) &traffic_light_program;

    ////////////////////////////////////////////////////////////////////////////////////////////
    // MAIN LOOP

    while (true)
    {
        // fade and update pwm channels
        uint8_t i;
        channel_t *ch = channels;
        for (i = 0; i < CHANNELS; ++i, ++ch)
            if (timer_expired(&ch->fade_timer))
            {
                timer_set(&ch->fade_timer, ch->fade_delay);
                if (!update_lum(ch))
                    fading_done |= 1 << i;
            }

        // process script
        execute_script_thread(&script_thread);
        uint8_t event = get_key_event() & 7;

        // dispatch key events
        if (event != 0)
        {
            uint16_t pos = script_thread.event_table[event - 1];
            if (pos != 0xffff)
            {
                script_thread.handler.position = pos;
                script_thread.reentry = 0;
                script_thread.event_table[event - 1] = 0xffff;
            }
        }
    }
}
