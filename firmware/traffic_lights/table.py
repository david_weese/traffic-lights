#!/usr/bin/python

levels = 256
interval_cycles = 256

if __name__ == "__main__":
    last = 0
    sum = 0
    for i in range(1,levels+1):
        cycles = (i/float(levels))**2*float(255)
        delta = round(cycles-last)
        print "%6d," % (round(cycles) % interval_cycles),
        if (i % 8 == 0):
          print
        last = cycles
        sum+=delta

#print "sum: %d" % sum
