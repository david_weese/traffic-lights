#if STATIC_SCRIPTS

#include "static_scripts.h"
#include "key.h"

////////////////////////////////////////////////////////////////////////////////////////////
// MAIN SCRIPT
//
// The main script has 2 modes:
//
// 1) the standard sequence:
//    RED -> RED+YELLOW -> GREEN -> YELLOW -> ... (repeat from the beginning)
//
// 2) the out-of-order sequence:
//    YELLOW on -> YELLOW off -> ... (repeat from the beginning)
//
// The keys red and green allow to fast forward to RED and GREEN states in the standard sequence.
// If both keys are pressed longer the out-of-order sequence is started and can be left with any key.

#define CHANNEL_GREEN   1
#define CHANNEL_YELLOW  2
#define CHANNEL_RED     4

static const uint8_t traffic_light_program[] PROGMEM = {

// initialization with short red phase, 8 instructions
    SLEEP(5),                                           // first sleep for short-circuit relief
    SET_CHANNEL(CHANNEL_GREEN | CHANNEL_YELLOW, 0),
    FADE_STEP(CHANNEL_GREEN | CHANNEL_YELLOW | CHANNEL_RED, 1),
    FADE_DELAY(CHANNEL_GREEN | CHANNEL_YELLOW | CHANNEL_RED, 1),
    FADE_CHANNEL(CHANNEL_RED, 255),
    EVENT_JUMP(KEY_GREEN, 20),                          // green pressed -> jump to (G1)
    EVENT_JUMP(KEY_RED, 34),                            // red pressed -> jump to (R2)
    EVENT_JUMP(KEY_RED | KEY_GREEN | KEY_LONG, 41),     // red + green pressed long -> jump to blink sequence
    SLEEP(30),                                          // RED, 3s

// standard loop begin, 16 instructions
    EVENT_JUMP(KEY_GREEN, 19),                          // green pressed -> jump to (G2)
    FADE_CHANNEL(CHANNEL_YELLOW, 255),                  // RED + YELLOW, 1.5s (StVO demands 1-2s)
    SLEEP(15),
    EVENT_JUMP(KEY_RED, 24),                            // red pressed -> jump to (R1)
    FADE_CHANNEL(CHANNEL_RED | CHANNEL_YELLOW, 0),      // GREEN, 15s
    FADE_CHANNEL(CHANNEL_GREEN, 255),
    SLEEP(150),
    EVENT_JUMP(KEY_RED, 24),                            // red pressed -> jump to (R2)
    FADE_CHANNEL(CHANNEL_GREEN, 0),                     // YELLOW, 3s (required for a speed of 50km/h)
    FADE_CHANNEL(CHANNEL_YELLOW, 255),
    SLEEP(30),
    EVENT_JUMP(KEY_GREEN, 5),                           // green pressed -> jump to (G1)
    FADE_CHANNEL(CHANNEL_YELLOW, 0),                    // RED, 15s
    FADE_CHANNEL(CHANNEL_RED, 255),
    SLEEP(150),
    JUMP(-15),

// long green phase, 11 instructions
    EVENT_JUMP(KEY_RED, 15),                            // (G1): red pressed -> jump to (R2)
    FADE_CHANNEL(CHANNEL_YELLOW, 255),                  // YELLOW, 1.5s
    SLEEP(15),
    EVENT_JUMP(KEY_RED, 8),                             // (G2): red pressed -> jump to (R1)
    EVENT_JUMP(KEY_RED | KEY_GREEN | KEY_LONG, 19),     // red + green pressed long -> jump to blink sequence
    FADE_CHANNEL(CHANNEL_RED | CHANNEL_YELLOW, 0),      // GREEN, 30s
    FADE_CHANNEL(CHANNEL_GREEN, 255),
    EVENT_JUMP(CHANNEL_GREEN, 2),                       // green pressed -> restart sleep
    EVENT_JUMP(CHANNEL_GREEN | KEY_LONG, 1),            // green pressed long -> restart sleep
    SLEEP(150),
    JUMP(-22),                                          // jump to sleep of "GREEN, 15s"

// long red phase, 12 instructions
    EVENT_JUMP(KEY_GREEN, -8),                          // (R1): green pressed -> jump to (G2)
    FADE_CHANNEL(CHANNEL_GREEN | CHANNEL_RED, 0),       // YELLOW, 3s
    FADE_CHANNEL(CHANNEL_YELLOW, 255),
    SLEEP(30),
    EVENT_JUMP(KEY_GREEN, -15),                         // (R2): green pressed -> jump to (G1)
    EVENT_JUMP(KEY_RED | KEY_GREEN | KEY_LONG, 7),      // red + green pressed long -> jump to blink sequence
    FADE_CHANNEL(CHANNEL_YELLOW | CHANNEL_GREEN, 0),    // RED, 15s
    FADE_CHANNEL(CHANNEL_RED, 255),
    EVENT_JUMP(KEY_RED, 2),                             // red pressed -> restart sleep
    EVENT_JUMP(KEY_RED | KEY_LONG, 1),                  // red pressed long -> restart sleep
    SLEEP(150),
    JUMP(-26),                                          // jump to sleep of "RED, 15s"

// yellow blink phase (out-of-order sequence), 10 instructions
    EVENT_JUMP(KEY_GREEN, -20),                         // (R1): green pressed -> jump to (G2)
    EVENT_JUMP(KEY_RED, -9),                            // (G1): red pressed -> jump to (R2)
    SET_CHANNEL(CHANNEL_GREEN | CHANNEL_RED, 0),

    FADE_STEP(CHANNEL_YELLOW, 2),
    FADE_CHANNEL(CHANNEL_YELLOW, 0),
    SLEEP(5),
    FADE_STEP(CHANNEL_YELLOW, 3),
    FADE_CHANNEL(CHANNEL_YELLOW, 255),
    SLEEP(5),
    JUMP(-6)

};

#endif
