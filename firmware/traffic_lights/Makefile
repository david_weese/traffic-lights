# microcontroller and project specific settings
TARGET = traffic_lights
F_CPU = 8000000UL
MCU = attiny45
AVRDUDE_MCU=t45
LIB = ../lib

OBJECTS += $(patsubst %.c,%.o,$(shell echo *.c $(LIB)/*.c))
HEADERS += $(shell echo *.h $(LIB)/*.h)
# CFLAGS += -Werror
LDFLAGS += -L/usr/local/avr/avr/lib
CFLAGS += -Wall -W -pedantic -DGAMMA_CORRECTION -DSTATIC_SCRIPTS -I$(LIB)

include $(LIB)/avr.mk

# no safe mode checks
#AVRDUDE_FLAGS += -u -F

.PHONY: all

all: $(TARGET).elf $(TARGET).eep.hex $(TARGET).lss

all.hex: $(TARGET).hex $(TARGET).eep.hex $(TARGET).lss

$(TARGET): $(OBJECTS) $(TARGET).o

%.o: $(HEADERS)

.PHONY: install

# install: program-serial-$(TARGET) program-serial-eeprom-$(TARGET)
install: program-isp-$(TARGET)

.PHONY: clean clean-$(TARGET) clean-botloader

clean: clean-$(TARGET)

clean-$(TARGET):
	$(RM) $(TARGET) $(OBJECTS)

clean-bootloader:
	$(MAKE) -C bootloader clean

.PHONY: bootstrap fuse install-bootloader

bootstrap: fuse install-bootloader install

# disable CKDIV8 -> CPU_CLOCK = 8MHz
fuse-attiny45:
	$(AVRDUDE) $(AVRDUDE_FLAGS) -c $(ISP_PROG) -P $(ISP_DEV) -U lfuse:w:0xe2:m -U hfuse:w:0xdf:m

install-bootloader: bootloader.hex program-isp-bootloader
