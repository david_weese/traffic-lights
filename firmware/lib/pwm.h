// File:   pwm.h
// Author: David Weese <dave.weese@gmail.com>

#ifndef PWM_H
#define PWM_H

#include "timer.h"

#define CHANNELS        3
#define CHANNEL0_PIN    _BV(DDB1)
#define CHANNEL1_PIN    _BV(DDB3)
#define CHANNEL2_PIN    _BV(DDB4)

extern uint8_t fading_done;
extern volatile uint8_t pwm_init_mask;

typedef struct
{
    uint8_t pwm_channel;
    uint8_t port_mask;

    uint8_t lum_current;
    uint8_t lum_target;

    uint8_t fade_delay;
    uint8_t fade_step;
    struct timer_t fade_timer;
} channel_t;

extern channel_t channels[CHANNELS];

bool update_lum(channel_t *ch);
void init_pwm();
void init_fader();

#endif  // ifndef PWM_H


