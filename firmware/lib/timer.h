// File:   timer.h
// Author: David Weese <dave.weese@gmail.com>

#ifndef TIMER_H
#define TIMER_H

#include <stdbool.h>
#include <stdint.h>

struct __attribute__((__packed__)) timer_t {
    uint16_t timeout;
    uint8_t  current;
};

void init_timer();
void timer_set(struct timer_t *t, uint16_t timeout);
bool timer_expired(struct timer_t *t);

#endif  // ifndef TIMER_H
