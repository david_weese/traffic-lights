/* File:   pwm.c
 * Author: David Weese <dave.weese@gmail.com>
 */

#include <stdio.h>
#include <avr/pgmspace.h>
#include <avr/interrupt.h>

#include "pwm.h"

volatile uint8_t pwm_init_mask;
volatile uint8_t pwm_level[CHANNELS];

uint8_t count;
uint8_t fading_done;

channel_t channels[CHANNELS];

#ifdef GAMMA_CORRECTION
static const uint8_t gamma[256] PROGMEM =
{
     0,      0,      0,      0,      0,      0,      0,      0,
     0,      0,      0,      1,      1,      1,      1,      1,
     1,      1,      1,      2,      2,      2,      2,      2,
     2,      3,      3,      3,      3,      4,      4,      4,
     4,      4,      5,      5,      5,      6,      6,      6,
     7,      7,      7,      8,      8,      8,      9,      9,
     9,     10,     10,     11,     11,     11,     12,     12,
    13,     13,     14,     14,     14,     15,     15,     16,
    16,     17,     17,     18,     19,     19,     20,     20,
    21,     21,     22,     22,     23,     24,     24,     25,
    26,     26,     27,     27,     28,     29,     29,     30,
    31,     32,     32,     33,     34,     34,     35,     36,
    37,     37,     38,     39,     40,     40,     41,     42,
    43,     44,     45,     45,     46,     47,     48,     49,
    50,     51,     51,     52,     53,     54,     55,     56,
    57,     58,     59,     60,     61,     62,     63,     64,
    65,     66,     67,     68,     69,     70,     71,     72,
    73,     74,     75,     76,     77,     78,     80,     81,
    82,     83,     84,     85,     86,     88,     89,     90,
    91,     92,     93,     95,     96,     97,     98,    100,
   101,    102,    103,    105,    106,    107,    109,    110,
   111,    112,    114,    115,    116,    118,    119,    121,
   122,    123,    125,    126,    127,    129,    130,    132,
   133,    135,    136,    138,    139,    140,    142,    143,
   145,    146,    148,    149,    151,    153,    154,    156,
   157,    159,    160,    162,    164,    165,    167,    168,
   170,    172,    173,    175,    177,    178,    180,    182,
   183,    185,    187,    188,    190,    192,    193,    195,
   197,    199,    200,    202,    204,    206,    208,    209,
   211,    213,    215,    217,    219,    220,    222,    224,
   226,    228,    230,    232,    234,    235,    237,    239,
   241,    243,    245,    247,    249,    251,    253,    255
};
#endif /// GAMMA_CORRECTION

// timer 0 output compare A interrupt
__attribute__((naked))
ISR(TIMER0_COMPA_vect)
{
    __asm__ __volatile (
        "push r18"                  "\t\t\t\t;  \n\t"
        "in r18,__SREG__"           "\t\t\t\t;  \n\t"
        "push r18"                  "\t\t\t\t;  \n\t"
        "push r24"                  "\t\t\t\t;  \n\t"
        "push r25"                  "\t\t\t\t;  \n\t"

        "lds r24,pwm_init_mask"     "\t\t\t;    \n\t"
        "lds r25,count"             "\t\t\t\t;  \n\t"
        "inc r25"                   "\t\t\t\t\t;\n\t"
        "sts count,r25"             "\t\t\t\t;  \n\t"

        "lds r18,pwm_level"         "\t\t\t\t;  \n\t"
        "cp r25,r18"                "\t\t\t\t;  \n\t"
        "brsh .CHANNEL0_OFF"        "\t\t\t\t;  \n\t"
        "ori r24,%[CHANNEL0_ON]"    "\t\t\t\t;  \n\n"

".CHANNEL0_OFF: \n\t"
        "subi r25, lo8(-85)"        "\t\t\t\t;  \n\t"
        "lds r18,pwm_level+1"       "\t\t\t\t;  \n\t"
        "cp r25,r18"                "\t\t\t\t;  \n\t"
        "brsh .CHANNEL1_OFF"        "\t\t\t;    \n\t"
        "ori r24,%[CHANNEL1_ON]"    "\t\t\t\t;  \n\n"

".CHANNEL1_OFF: \n\t"
        "subi r25, lo8(-85)"        "\t\t\t\t;  \n\t"
        "lds r18,pwm_level+2"       "\t\t\t\t;  \n\t"
        "cp r25,r18"                "\t\t\t\t;  \n\t"
        "brsh .CHANNEL2_OFF"        "\t\t\t\t;  \n\t"
        "ori r24,%[CHANNEL2_ON]"    "\t\t\t\t;  \n\n"

".CHANNEL2_OFF: \n\t"
        "out 0x18,r24"              "\t\t\t\t;  \n\t"

        "pop r25"                   "\t\t\t\t\t;\n\t"
        "pop r24"                   "\t\t\t\t\t;\n\t"
        "pop r18"                   "\t\t\t\t\t;\n\t"
        "out __SREG__,r18"          "\t\t\t;    \n\t"
        "pop r18"                   "\t\t\t\t\t;\n\t"
        "reti\n"
        :
        :       [CHANNEL0_ON] "i" (CHANNEL0_PIN),
                [CHANNEL1_ON] "i" (CHANNEL1_PIN),
                [CHANNEL2_ON] "i" (CHANNEL2_PIN)
    );
}

//ISR(TIMER0_COMPA_vect)
//{
//    register uint8_t m = pwm_init_mask;
//    if (pwm_level[0] > count) m |= CHANNEL0_PIN;
//    if (pwm_level[1] > count) m |= CHANNEL1_PIN;
//    if (pwm_level[2] > count) m |= CHANNEL2_PIN;
//    PORTB = m;
//    ++count;
//}

void update_level(channel_t *ch)
{
    uint8_t level = ch->lum_current;
#ifdef GAMMA_CORRECTION
    level = pgm_read_byte(&(gamma[level]));
#endif
    if (level == 255)
        pwm_init_mask |= ch->port_mask;
    else
        pwm_init_mask &= ~ch->port_mask;
    pwm_level[ch->pwm_channel] = level;
}

bool update_lum(channel_t *ch)
{
    if (ch->lum_current == ch->lum_target)
    {
        update_level(ch);
        return false;
    }

    if (ch->lum_current < ch->lum_target)
    {
        if (ch->lum_target - ch->lum_current > ch->fade_step)
            ch->lum_current += ch->fade_step;
        else
            ch->lum_current = ch->lum_target;
    }
    else
    {
        if (ch->lum_current - ch->lum_target > ch->fade_step)
            ch->lum_current -= ch->fade_step;
        else
            ch->lum_current = ch->lum_target;
    }

    update_level(ch);
    return true;
}

void init_pwm()
{
    uint8_t i;
    for (i = 0; i < CHANNELS; ++i)
        pwm_level[i] = 0;
    pwm_init_mask = 0;
    count = 0;

    // set all LED ports to output
    DDRB |= CHANNEL0_PIN | CHANNEL1_PIN | CHANNEL2_PIN;

    // timer 0 for PWM
    TCCR0A = (2 << WGM00);              // put timer 0 in CTC mode
    TCCR0B = (1 << CS00);               // timer 0 prescaling - divides by 1024
    TIMSK = (1 << OCIE0A);              // enable timer compare interrupt
    TCNT0 = 0;
    OCR0A = 255;                        // a PWM frequency of 120Hz is enough for us
}

void init_fader()
{
    uint8_t i;
    for (i = 0; i < CHANNELS; ++i)
    {
        channel_t *ch = channels + i;
        ch->lum_current = 0;
        ch->lum_target = 0;
        ch->fade_step = 1;
        ch->fade_delay = 1;
        ch->pwm_channel = i;
        timer_set(&ch->fade_timer, ch->fade_delay);
    }
    channels[0].port_mask = CHANNEL0_PIN;
    channels[1].port_mask = CHANNEL1_PIN;
    channels[2].port_mask = CHANNEL2_PIN;
    fading_done = 7;
}

