/* vim:fdm=marker ts=4 et ai
 * {{{
 *         fnordlicht firmware next generation
 *
 *    for additional information please
 *    see http://koeln.ccc.de/prozesse/running/fnordlicht
 *
 * (c) by Alexander Neumann <alexander@bumpern.de>
 *     Lars Noschinski <lars@public.noschinski.de>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 *
 * For more information on the GPL, please go to:
 * http://www.gnu.org/copyleft/gpl.html
 }}} */

// edited by David Weese <dave.weese@gmail.com>

#ifndef fnordlicht_static_script_h
#define fnordlicht_static_script_h

#if STATIC_SCRIPTS

#include <stdint.h>
#include <avr/io.h>
#include <avr/eeprom.h>
#include <avr/pgmspace.h>

#include "timer.h"

/* opcodes */
#define OP_NOP              0x00
#define OP_SET_CHANNEL      0x10
#define OP_FADE_CHANNEL     0x20
#define OP_FADE_DELAY       0x30
#define OP_FADE_STEP        0x80
#define OP_JUMP             0x40
#define OP_SLEEP            0x50
#define OP_WAIT             0x60
#define OP_STOP             0x70

/* opcode macros */
#define NOP() \
    OP_NOP, 0

#define SET_CHANNEL(channel, target_brightness) \
    (OP_SET_CHANNEL | channel), target_brightness

#define FADE_CHANNEL(channel, target_brightness) \
    (OP_FADE_CHANNEL | channel), target_brightness

#define FADE_DELAY(channel, delay) \
    (OP_FADE_DELAY | channel), delay

#define FADE_STEP(channel, step) \
    (OP_FADE_STEP | channel), step

#define JUMP(offset) \
     OP_JUMP, offset

#define EVENT_JUMP(key, offset) \
    (OP_JUMP | key), offset

#define SLEEP(delay100ms) \
     OP_SLEEP, delay100ms

#define WAIT(eventmask) \
     OP_WAIT, eventmask

#define STOP() \
     OP_STOP, 0

/* opcode function return values */
#define OP_RETURN_OK        0   /* proceed with next cycle */
#define OP_RETURN_WAIT      1   /* execution has been completed for this cycle, jump to next thread */
#define OP_RETURN_STOP      2   /* disable this script */

/* workaround prototype for handler 'execute' function definition */
struct thread_t;

/* structs */
struct script_handler_t
{
    uint8_t (*execute)(struct thread_t *);
    uint16_t position;
};

struct thread_t
{
    struct script_handler_t handler;
    uint16_t event_table[7];
    struct timer_t sleep_timer;
    bool reentry;               // true for opcodes that are executed another time
};

/* prototypes */
void init_script_thread(struct thread_t *thread);
uint8_t execute_script_thread(struct thread_t *thread);

/* memory handlers */
uint8_t memory_handler_flash(struct thread_t *thread);
uint8_t memory_handler_eeprom(struct thread_t *thread);

#endif

#endif
