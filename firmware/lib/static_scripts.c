/* vim:fdm=marker ts=4 et ai
 * {{{
 *         fnordlicht firmware next generation
 *
 *    for additional information please
 *    see http://koeln.ccc.de/prozesse/running/fnordlicht
 *
 * (c) by Alexander Neumann <alexander@bumpern.de>
 *     Lars Noschinski <lars@public.noschinski.de>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 *
 * For more information on the GPL, please go to:
 * http://www.gnu.org/copyleft/gpl.html
 }}} */

// edited by David Weese <dave.weese@gmail.com>

#if STATIC_SCRIPTS

#include <stdint.h>
#include <avr/pgmspace.h>
#include <util/delay.h>

#include "pwm.h"
#include "static_scripts.h"

/* opcode handlers */
uint8_t opcode_handler_nop(uint8_t parameters[], struct thread_t *thread);
uint8_t opcode_handler_invalid(uint8_t parameters[], struct thread_t *thread);
uint8_t opcode_handler_set_channel(uint8_t parameters[], struct thread_t *thread);
uint8_t opcode_handler_fade_channel(uint8_t parameters[], struct thread_t *thread);
uint8_t opcode_handler_jump(uint8_t parameters[], struct thread_t *thread);
uint8_t opcode_handler_sleep(uint8_t parameters[], struct thread_t *thread);
uint8_t opcode_handler_wait(uint8_t parameters[], struct thread_t *thread);
uint8_t opcode_handler_stop(uint8_t parameters[], struct thread_t *thread);
uint8_t opcode_handler_set_fade_delay(uint8_t parameters[], struct thread_t *thread);
uint8_t opcode_handler_set_fade_step(uint8_t parameters[], struct thread_t *thread);

/* opcode lookup table */
uint8_t (*opcode_lookup_table[])(uint8_t parameters[], struct thread_t *thread) = {
    &opcode_handler_nop,            /* opcode 0x00 */
    &opcode_handler_set_channel,    /* opcode 0x10 */
    &opcode_handler_fade_channel,   /* opcode 0x20 */
    &opcode_handler_set_fade_delay, /* opcode 0x30 */
    &opcode_handler_jump,           /* opcode 0x40 */
    &opcode_handler_sleep,          /* opcode 0x50 */
    &opcode_handler_wait,           /* opcode 0x60 */
    &opcode_handler_stop,           /* opcode 0x70 */
    &opcode_handler_set_fade_step,  /* opcode 0x80 */
    &opcode_handler_invalid,        /* opcode 0x90 */
    &opcode_handler_invalid,        /* opcode 0xA0 */
    &opcode_handler_invalid,        /* opcode 0xB0 */
    &opcode_handler_invalid,        /* opcode 0xC0 */
    &opcode_handler_invalid,        /* opcode 0xD0 */
    &opcode_handler_invalid,        /* opcode 0xE0 */
    &opcode_handler_invalid         /* opcode 0xF0 */
};


/* init all structures in the global array 'script_threads' */
void init_script_thread(struct thread_t *thread)
{
    uint8_t i;

    thread->handler.execute = 0;
    thread->handler.position = 0;
    thread->reentry = 0;
    for (i = 0; i < 7; ++i)
        thread->event_table[i] = 0xffff;
}

/* iterate over all threads and execute each, if enabled */
uint8_t execute_script_thread(struct thread_t *thread)
{
    return thread->handler.execute(thread);
}

/* memory handlers */
uint8_t memory_handler_flash(struct thread_t *thread)
{
    uint8_t opcode;
    uint8_t parameters[2];
    uint8_t result;

    do {
        /* read opcode and parameters and call the appropiate opcode processing function */
        opcode = pgm_read_byte((uint8_t *)(thread->handler.position)++);

        /* safe flags as first parameter */
        parameters[0] = (opcode & 0x0f);

        /* extract real opcode */
        opcode >>= 4;

        /* load other parameters */
        parameters[1] = pgm_read_byte((uint8_t *)(thread->handler.position)++);

        /* call opcode handler */
        result = (opcode_lookup_table[opcode])(parameters, thread);

        if ((thread->reentry = (result == OP_RETURN_WAIT)))
            thread->handler.position -= 2;

    } while (result == OP_RETURN_OK);
    return result;
}

uint8_t memory_handler_eeprom(struct thread_t *thread)
{
    uint8_t opcode;
    uint8_t parameters[2];
    uint8_t result;

    do {
        /* read opcode and parameters and call the appropiate opcode processing function */
        opcode = eeprom_read_byte((uint8_t *)(thread->handler.position)++);

        /* safe flags as first parameter */
        parameters[0] = (opcode & 0x0f);

        /* extract real opcode */
        opcode >>= 4;

        /* load other parameters */
        parameters[1] = eeprom_read_byte((uint8_t *)(thread->handler.position)++);

        /* call opcode handler */
        result = (opcode_lookup_table[opcode])(parameters, thread);

        if ((thread->reentry = (result == OP_RETURN_WAIT)))
            thread->handler.position -= 2;

    } while (result == OP_RETURN_OK);
    return result;
}

/* opcode handlers */
uint8_t opcode_handler_nop(uint8_t parameters[], struct thread_t *thread)
{
    (void) parameters;
    (void) thread;

    /* do exactly nothing */
    return OP_RETURN_OK;
}

uint8_t opcode_handler_invalid(uint8_t parameters[], struct thread_t *thread)
{
    (void) parameters;
    (void) thread;

    uint8_t i = 0;
    for (i = 0; i < 3; ++i)
    {
        channels[i].lum_target = 255;
        channels[i].lum_current = 255;
    }

    return OP_RETURN_OK;
}

uint8_t opcode_handler_fade_channel(uint8_t parameters[], struct thread_t *thread)
{
    (void) thread;

    fading_done &= ~parameters[0];

    channel_t *ch;
    for (ch = channels; parameters[0] != 0; ++ch, parameters[0] >>= 1)
        if (parameters[0] & 1)
            ch->lum_target = parameters[1];
    return OP_RETURN_OK;
}

uint8_t opcode_handler_set_channel(uint8_t parameters[], struct thread_t *thread)
{
    (void) thread;

    channel_t *ch;
    for (ch = channels; parameters[0] != 0; ++ch, parameters[0] >>= 1)
        if (parameters[0] & 1)
        {
            ch->lum_target = parameters[1];
            ch->lum_current = parameters[1];
        }
    return OP_RETURN_OK;
}

uint8_t opcode_handler_set_fade_delay(uint8_t parameters[], struct thread_t *thread)
{
    (void) thread;

    channel_t *ch;
    for (ch = channels; parameters[0] != 0; ++ch, parameters[0] >>= 1)
        ch->fade_delay = parameters[1];
    return OP_RETURN_OK;
}

uint8_t opcode_handler_set_fade_step(uint8_t parameters[], struct thread_t *thread)
{
    (void) thread;

    channel_t *ch;
    for (ch = channels; parameters[0] != 0; ++ch, parameters[0] >>= 1)
        ch->fade_step = parameters[1];
    return OP_RETURN_OK;
}

uint8_t opcode_handler_jump(uint8_t parameters[], struct thread_t *thread)
{
    int16_t offset = ((int8_t) parameters[1]);

    /* correct offset so that -1 really jumps to the instruction _before_ this one */
    offset--;

    /* comput the byte offset */
    offset *= 2;

    if (parameters[0] == 0)
        // jump immediately
        thread->handler.position += offset;
    else
        // store jump address in event table
        thread->event_table[parameters[0] - 1] = thread->handler.position + offset;

    return OP_RETURN_OK;
}

uint8_t opcode_handler_sleep(uint8_t parameters[], struct thread_t *thread)
{
    if (thread->reentry)
    {
        if (timer_expired(&thread->sleep_timer))
            return OP_RETURN_OK;
    }
    else
    {
        timer_set(&thread->sleep_timer, 100 * (uint16_t)parameters[1]);
    }
    return OP_RETURN_WAIT;
}

extern uint8_t fading_done;

uint8_t opcode_handler_wait(uint8_t parameters[], struct thread_t *thread)
{
    (void) thread;

    if ((fading_done & parameters[1]) == parameters[1])
        return OP_RETURN_OK;
    else
        return OP_RETURN_WAIT;
}

uint8_t opcode_handler_stop(uint8_t parameters[], struct thread_t *thread)
{
    (void) parameters;
    (void) thread;

    return OP_RETURN_STOP;
}



#endif
