/* File:   timer.c
 * Author: David Weese <dave.weese@gmail.com>
 */

#include "timer.h"
#include <avr/io.h>

void init_timer()
{
    // set timer 1 to a resolution of 1ms
    TCCR1 = (14 << CS10);  // divide by 8192
    TCNT1 = 0;
}

void timer_set(struct timer_t *t, uint16_t timeout)
{
    t->current = TCNT1;
    t->timeout = timeout;
}

bool timer_expired(struct timer_t *t)
{
    while (true)
    {
        if (t->timeout == 0)
            return true;

        if (t->current != TCNT1)
        {
            --t->timeout;
            ++t->current;
        }
        else
        {
            return false;
        }
    }
}
