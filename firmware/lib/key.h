// File:   key.h
// Author: David Weese <dave.weese@gmail.com>

#ifndef KEY_H
#define KEY_H

#define KEY_PIN         PINB

#define KEY_GREEN       1
#define KEY_LONG        2
#define KEY_RED         4
#define KEYS_ALL        (KEY_GREEN | KEY_RED)
 
#define REPEAT_MASK     KEYS_ALL    // repeat: all keys
#define REPEAT_START    40          // after 400ms
#define REPEAT_NEXT     20          // every 200ms
 
void init_keys();
uint8_t get_key_event();

#endif  // ifndef KEY_H

