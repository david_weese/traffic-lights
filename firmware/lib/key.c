/* File:   key.c
 * Author: Peter Dannegger <danni@specs.de>
 *         David Weese <dave.weese@gmail.com>
 */

#include <stdint.h>
#include <avr/io.h>
#include <avr/interrupt.h>

#include "key.h"
#include "timer.h"
#include "pwm.h"

volatile uint8_t key_state;         // debounced and inverted key state (1: key pressed)
volatile uint8_t key_press;         // key press detect
volatile uint8_t key_rpt;           // key long press and repeat

static uint8_t ct0, ct1, rpt;

struct timer_t key_timer;

void init_keys()
{
    key_state = 0;
    key_press = 0;
    key_rpt = 0;

    ct0 = 0xff;
    ct1 = 0xff;
    rpt = REPEAT_START;

    DDRB &= ~KEYS_ALL;              // configure key port for input
//    PORTB |= KEYS_ALL;               // enable pull-ups
    pwm_init_mask = KEYS_ALL;       // enable pull-ups
    timer_set(&key_timer, 10);
}

void update_keys()
{
    uint8_t i;

    if (timer_expired(&key_timer))
    {
        timer_set(&key_timer, 10);

        i = key_state ^ ~(KEY_PIN & KEYS_ALL);          // key changed ?
        ct0 = ~( ct0 & i );                             // reset or count ct0
        ct1 = ct0 ^ (ct1 & i);                          // reset or count ct1
        i &= ct0 & ct1;                                 // count until roll over ?
        key_state ^= i;                                 // then toggle debounced state
        key_press |= key_state & i;                     // 0->1: key press detect

        if ((key_state & REPEAT_MASK) == 0)             // check repeat function
            rpt = REPEAT_START;                         // start delay

        if (--rpt == 0)
        {
            rpt = REPEAT_NEXT;                          // repeat delay
            key_press |= key_state | KEY_LONG;
        }
    }
}

uint8_t get_key_event()
{
    update_keys();
    uint8_t result = key_press;                         // read key(s)
    key_press = 0;                                      // clear key(s)
    return result;
}
